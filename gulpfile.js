var gulp = require('gulp');
var babel = require('gulp-babel');
var replace = require('gulp-replace');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var concat = require('gulp-concat-util');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var replace = require('gulp-replace');
var sourcemaps = require('gulp-sourcemaps');
var sassGlob = require('gulp-sass-glob');
var browserSync = require('browser-sync').create();
var jsSource, scssSource;
var download = require("gulp-download");
var rename = require("gulp-rename");
var css = require('css');
var removeEmptyLines= require("remove-blank-lines");
var stripCssComments = require('strip-css-comments');

scssSource = ['scss/template.scss','scss/*/*.scss','scss/*/*/*.scss','scss/*.scss'];
jsSource = ['js/*.js','js/*/*.js','js/*/*/*.js'];
config =['config.js'];

var user=process.argv[3];
if(user==undefined){
    user="global";
}

var plugins = require('gulp-load-plugins')();
var map = require('map-stream');
var events = require('events');
var emitter = new events.EventEmitter();
var path = require('path');
var gutil = require('gulp-util');
var currentTask="";




console.log("");
console.log("");
console.log("##############################################");
console.log('PLIKI BEDA GENEROWANE W KATALOGU : build/'+user);
console.log("##############################################");

gulp.task('scss', function() {
    currentTask="scss";
    var versionDate = new Date();
    gulp.src("scss/template.scss")
        .pipe(sassGlob())
        .pipe(sourcemaps.init())
        .on('error', swallowError)
        .pipe(sass())
        .on('error', swallowError)
        .pipe(replace('{date}', versionDate))
        .on('error', swallowError)
        .pipe(autoprefixer({ browsers: ['>0%']}))
        .on('error', swallowError)
        .pipe(replace("/*==",""))
        .pipe(replace("==*/",""))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('build/'+user+'/css'))
        .pipe(browserSync.stream({match: '**/template.css'}));
});
gulp.task('init_server',function(){
        currentTask="init_server";

    browserSync.init({
        server: {
            baseDir: 'build/'+user+'/',
            files: ['build/'+user+'/css/template.css','build/'+user+'/css/template.js']
        },
        open: false
    });
});
gulp.task('js', function() {

        currentTask="js";
    return gulp.src(jsSource)
        .pipe(sourcemaps.init())
        .pipe(concat.header('// file: <%= file.relative %>\n'))
        .pipe(concat.footer('\n// end file: <%= file.relative %>\n'))
        .pipe(concat('template.js'))
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('jshint-stylish'))
        .on('error', swallowError)
        .pipe(babel())
        .on('error', swallowError)
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('build/'+user+'/js'));
});

gulp.task('config_change',function(){
        currentTask="config_change";
    var fs = require('fs');
    fs.readFile('./config.js', function (err, data) {
    var config_json=JSON.parse(data.toString());
    var replace_js= new RegExp("src[=][\"\'](.*?)("+config_json.js_replace+")[\"\']","g");
    var replace_css= new RegExp("href[=][\"\'](.*?)("+config_json.css_replace+")[\"\']","g");
    download(config_json.download_url)
        .pipe(rename("index.html"))
        .pipe(replace(replace_css,"href='css/template.css'"))
        .pipe(replace(replace_js,"src='js/template.js'"))
        .pipe(gulp.dest('build/'+user+'/'));

});

});

gulp.task('default', ['init_server','scss','js','config_change'], function() {
    watch(scssSource,function(){
        gulp.start('scss');

    });

    watch(jsSource,function(){
        gulp.start('js');
    });
    watch(config,function(){
       gulp.start('config_change');
     });

    watch("build/"+user+"/index.html",function(){
       browserSync.reload();
    });
    gulp.watch("build/"+user+"/js/template.js").on('change', browserSync.reload);
    var fs = require('fs');
        fs.readFile('./build/'+user+'/css/template.css', function (err, data) {
            if(data.toString().search("{%")>-1){
                    var file=data.toString();
                    file=file.replace(/{{/g,"#-");
                    file=file.replace(/}}/g,"-#");
                    file=file.replace(/{%/g,"/*{%");
                    file=file.replace(/%}/g,"%}*/");
                    var obj = css.parse(file);
                    var obj2 = css.parse(file);
                    for(var i=0;i<obj["stylesheet"]["rules"].length;i++){
                        if(obj["stylesheet"]["rules"][i].type=="rule"){
                            var newDeclaration=[];
                            for(var j=0;j<obj["stylesheet"]["rules"][i]["declarations"].length;j++){
                                
                                var value=String(obj["stylesheet"]["rules"][i]["declarations"][j]["value"]);
                                if(value.search("-#")>-1){
                                    newDeclaration.push(obj["stylesheet"]["rules"][i]["declarations"][j]);
                                    obj2["stylesheet"]["rules"][i]["declarations"][j]["type"]="comment";
                                }else{
                                    obj2["stylesheet"]["rules"][i]["declarations"][j]["type"]="declaration";
                                }
                            }
                            obj["stylesheet"]["rules"][i]["declarations"]=newDeclaration;
                        }else if(obj["stylesheet"]["rules"][i].type=="media"){
                            if(obj["stylesheet"]["rules"][i]["rules"]!=undefined){
                                var comment=0;
                                for(var k=0;k<obj["stylesheet"]["rules"][i]["rules"].length;k++){
                                    if(obj["stylesheet"]["rules"][i]["rules"][k]["declarations"]!=undefined){
                                    var newDeclaration=[];
                                        for(var j=0;j<obj["stylesheet"]["rules"][i]["rules"][k]["declarations"].length;j++){
                                            
                                            var value=String(obj["stylesheet"]["rules"][i]["rules"][k]["declarations"][j]["value"]);
                                            if(value.search("-#")>-1){
                                                newDeclaration.push(obj["stylesheet"]["rules"][i]["rules"][k]["declarations"][j]);
                                                obj2["stylesheet"]["rules"][i]["rules"][k]["declarations"][j]["type"]="comment";
            //                                    j--;
                                            }else{
                                                obj2["stylesheet"]["rules"][i]["rules"][k]["declarations"][j]["type"]="declaration";
                                            }
                                        }
                                    obj["stylesheet"]["rules"][i]["rules"][k]["declarations"]=newDeclaration;
                                    if(newDeclaration.length==0){
                                        obj["stylesheet"]["rules"][i]["rules"][k]["type"]=="comment";
                                        comment++;
                                    }
                                    }
                                }
                                if(comment==obj["stylesheet"]["rules"][i]["rules"].length){
                                    obj["stylesheet"]["rules"][i].type="comment";
                                }
                            }
                        }else if(obj["stylesheet"]["rules"][i].type=="keyframes"){
                           if(obj["stylesheet"]["rules"][i]["keyframes"]!=undefined){
                                var comment=0;
                                for(var k=0;k<obj["stylesheet"]["rules"][i]["keyframes"].length;k++){
                                    if(obj["stylesheet"]["rules"][i]["keyframes"][k]["declarations"]!=undefined){
                                    var newDeclaration=[];
                                        for(var j=0;j<obj["stylesheet"]["rules"][i]["keyframes"][k]["declarations"].length;j++){
                                            
                                            var value=String(obj["stylesheet"]["rules"][i]["keyframes"][k]["declarations"][j]["value"]);

                                            if(value.search("-#")>-1){
                                                newDeclaration.push(obj["stylesheet"]["rules"][i]["keyframes"][k]["declarations"][j]);
                                                obj2["stylesheet"]["rules"][i]["rules"][k]["declarations"][j]["type"]="comment";
            //                                    j--;
                                            }else{
                                                obj2["stylesheet"]["rules"][i]["keyframes"][k]["declarations"][j]["type"]="declaration";
                                            }
                                        }
                                    obj["stylesheet"]["rules"][i]["keyframes"][k]["declarations"]=newDeclaration;
                                    if(newDeclaration.length==0){
                                        obj["stylesheet"]["rules"][i]["keyframes"][k]["type"]=="comment";
                                        comment++;
                                    }
                                    }
                                }
                                if(comment==obj["stylesheet"]["rules"][i]["keyframes"].length){
                                    obj["stylesheet"]["rules"][i].type="comment";
                                }
                            }
                        }
                    }       
                    var cssStringHubSpot=css.stringify(obj).replace(/\/[*][!]/g,"/*");
                    cssStringHubSpot=cssStringHubSpot.replace(/#-/g,"{{");
                    cssStringHubSpot=cssStringHubSpot.replace(/-#/g,"}}");
                    cssStringHubSpot=cssStringHubSpot.replace(/\/[*]{%/g,"{%");
                    cssStringHubSpot=cssStringHubSpot.replace(/%}[*]\//g,"%}");   

                    var cssStringCDN=css.stringify(obj2).replace(/\/[*][!]/g,"/*");

                var fs2 = require('fs');
                fs2.writeFile('./build/'+user+'/css/template_for_hubspot.css',removeEmptyLines(stripCssComments(cssStringHubSpot)),function(){
                });
                fs2.writeFile('./build/'+user+'/css/template_for_cdn.css',removeEmptyLines(stripCssComments(cssStringCDN)),function(){
                });
            }
        });
});

var swallowError = function (error) {

    var lineNumber = error.line;
    var pluginName = "["+currentTask+"]";
    if(currentTask=="scss"){
        plugins.notify({
            title: 'Failed: ' +pluginName+ " ["+error.relativePath+"] on line "+lineNumber,
            message: error.messageOriginal
        }).write(error);
    }else if(currentTask=="js"){
        plugins.notify({
            title: 'Failed: ' +pluginName + "[js/template.js]",
            message: error.messageOriginal
        }).write(error);
    }

    gutil.beep();

    var report = '';
    var chalk = gutil.colors.white.bgRed;

    report += chalk('TASK:') + pluginName+'\n';
    report += chalk('ERROR:') + ' ' + error.message + '\n';
    if (error.line) { report += chalk('LINE:') + ' ' + error.line + '\n'; }
    if (error.fileName) { report += chalk('FILE:') + ' ' + error.fileName + '\n'; }

    console.error(report);

    this.emit('end');
}