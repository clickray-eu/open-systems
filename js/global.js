function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}


function hideEmptyLabel() {
    $('form label').each(function (i, e) {
        if ($(this).text() == "*") {
            $(this).css('display', 'none');
        }
    })
}

waitForLoad(".form, .widget-type-form, .widget-type-blog_content, .hs_cos_wrapper_type_form, .widget-type-blog_subscribe, .widget-type-blog_comments", "form", hideEmptyLabel);
waitForLoad(".form, .widget-type-form, .widget-type-blog_content, .subscribe-module", "form", select);

$(window).load(function () {
    formPopup('.btn-subscribe', '.subscribe-popup');
    formPopup('.btn-contact-sales', '.contact-sales-popup');
    formPopup('.btn-contact-footer','.contact-footer-popup');
    formPopup('.btn-contact','.contact-footer-popup');
});



