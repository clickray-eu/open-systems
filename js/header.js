$(document).ready(function () {

	slickInit();
	stickyNav();

})


function slickInit() {

	var combinedMenu = $('.menu-left .hs-menu-wrapper > ul').clone();
	var secondMenu = $('.menu-right .hs-menu-wrapper > ul').clone();

	secondMenu.children('li').appendTo(combinedMenu);

	combinedMenu.slicknav({
		duplicate: false,
		label: '',
		prependTo: '.os-menu-wrapper .container',
		openedSymbol: '<i class="fa fa-angle-down" aria-hidden="true"></i>',
		closedSymbol: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
		removeClasses: true,
		brand: 'Open Systems',
		allowParentLinks: true
	});

}

function stickyNav() {
	var menuOffsetTop = $('.os-menu-wrapper').offset().top;
	var menuHeight = $('.os-menu-wrapper').outerHeight();
	$('.os-menu-wrapper').parent().css('height', menuHeight);

	$(window).resize();
	if (menuOffsetTop > 0 && $(window).width() > 991) {
		let menuItem = $('.os-menu-wrapper .hs-menu-depth-2.hs-item-has-children > ul');
		let menuItemParentWidth = menuItem.parent().width();

		$(document).scroll(function () {
			var y = $(this).scrollTop();

			if (y > menuOffsetTop) {
				$('.os-menu-wrapper').addClass('sticky');
			} else {
				$('.os-menu-wrapper').removeClass('sticky');
			}
		}).scroll();
	} else {
		console.log()
		$('.os-menu-wrapper').addClass('sticky');
		$('.slide-content-wrapper').css('margin-top', menuHeight);
		// $('.os-menu-wrapper').parent().css('max-height', 0);


	}



}