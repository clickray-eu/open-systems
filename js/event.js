function toTheTop() {
    var el = $('#back-to-top-arrow');
    var offset = $(window).scrollTop();
    var windowHeight = $(window).height();
    var windowWidth = $(window).innerWidth();
    var documentHeight = $(document).height();
    var sum = offset + windowHeight + 150;
    if (offset >= windowHeight + 100 && $(window).innerWidth() > 768 && sum <= documentHeight) {
        el.fadeIn();
    } else {
        el.fadeOut();
    }
}

function withdrawTweet(twitterIframe) {
    var tweetIndex = 0,
        tweetWrap = $(twitterIframe),
        tweetIframe = tweetWrap.find('iframe'),
        tweetWrapper = tweetIframe.contents().find('.timeline-TweetList-tweet'),
        tweetCustom = tweetWrap.find('.tweets-wrapper .tweet-content');

    tweetWrapper.each(function () {
        var tweetText = $(this).contents().find('.timeline-Tweet-text').html(),
            tweetDate = $(this).contents().find('.timeline-Tweet-metadata time').attr('datetime').split(/T|\+|\-|\:/),
            tweetAvatarSrc = $(this).contents().find('.Avatar.Avatar--edge').attr('src'),
            tweetName = $(this).contents().find('.TweetAuthor-name').html(),
            tweetNameShort = $(this).contents().find('.TweetAuthor-screenName').html(),
            tweetDateString = tweetDate[1] + '/' + tweetDate[2] + '/' + tweetDate[0][2] + tweetDate[0][3]   + ', ' + tweetDate[3] + ':' + tweetDate[4];
        tweetCustom.eq(tweetIndex).find('.tweet-name').html(tweetName);
        tweetCustom.eq(tweetIndex).find('.tweet-name-short').html(tweetNameShort);
        tweetCustom.eq(tweetIndex).find('.tweet-post p').html(tweetText);
        tweetCustom.eq(tweetIndex).find('.tweet-date p').html(tweetDateString);
        tweetCustom.eq(tweetIndex).find('.tweet-avatar img').attr('src', tweetAvatarSrc);

        tweetIndex++;
    })
    tweetWrap.find('.loader').fadeOut();
}



function getScrollBarWidth() {
    var $outer = $('<div>').css({ visibility: 'hidden', width: 100, overflow: 'scroll' }).appendTo('body'),
        widthWithScroll = $('<div>').css({ width: '100%' }).appendTo($outer).outerWidth();
    $outer.remove();
    return 100 - widthWithScroll;
};

function matchHeight(elDesiredHeight, elToChange) {
    if ($(window).innerWidth() > 767) {
        var wantedHeight = $(elDesiredHeight).height();
        $(elToChange).height(wantedHeight);
    } else {
        $(elToChange).height('auto');
    }
}
$(window).load(function () {
    var interval = setInterval(function () {
        if ($('.twitter-box iframe').contents().find('.timeline-TweetList-tweet').length > 0) {
            clearInterval(interval);
            withdrawTweet('.twitter-box');
        }
    }, 50);
})

$(document).ready(function () {
    "use strict";
    toTheTop();

    if($('body').hasClass('stories')){
        $('.os-menu ul li a').filter(function(){
            return $(this).text()==='Stories';
        }).parent().addClass('active-branch');
    }
        if($('body').hasClass('about')){
        $('.os-menu ul li a').filter(function(){
            return $(this).text()==='About';
        }).parent().addClass('active-branch');
    }
    if ($('.blog-stories-single:not(.about-single)')) {
        setTimeout(function () {
            $(window).on('resize', function () {
                matchHeight('.blog-content', '.blog-sidebar-left')
            }).resize();
        }, 50);
    }


if ($('.fancybox')) {
    $('.fancybox').fancybox({
        padding: 0,
        onStart: function () {
            if ($(document).width() > $(window).width()) {
                $('body').addClass('fancybox-lock');
                $('body').addClass('fancybox-margin');
            } else {
                $('body').addClass('fancybox-lock');
            }
        },
        onClosed: function () {
            $('body').removeClass('fancybox-lock fancybox-margin');
        },
    });
}

$(window).scroll(function () {
    toTheTop();
});

$('#back-to-top, #back-to-top-arrow').on('click', function () {
    $('body, html').animate({
        scrollTop: 0,
    }, 500);
});
if ($('.sidebar .subnav')) {
    $('.sidebar .subnav ul li a').each(function () {
        var txt = $(this).text();
        $(this).html('<span>' + txt + '</span>');
    })
}
// ------ scroll do # w linkach ------


$('a[href*="#"]:not([href="#"]):not(.topic-link):not(.switch)').on('click', function (e) {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
            scrollTop: $(this.hash).offset().top
        }, 500);
        return false;
      }
    }
});
    // ------ scroll do # w linkach end ------
});


