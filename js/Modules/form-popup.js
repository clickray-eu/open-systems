function formPopup(button, modal) {
    var button = $(button),
        modal = $(modal),
        form = $('.subscribe-module .form-wrapper'),
        exit = $('.exit-btn'),
        body = $('body'),
        afterLoad = $('a[name="' + location.hash.replace('#', '') + '"]');

    

    $(button).on('click', function (e) {
        e.preventDefault;
        modal.fadeIn();
        body.css({
            'overflow' : 'hidden'
        });
    });

    $(exit).on('click', function () {
        modal.fadeOut();
        body.css({
            'overflow' : 'scroll'
        })
    });

    form.on('click', function (e) {
        e.stopPropagation();
    });

    if ($('a[name="' + location.hash.replace('#', '') + '"]')) {
        afterLoad.closest(modal).fadeIn(700);
    }
}

