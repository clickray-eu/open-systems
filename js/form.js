// Functions for dropdown
function select(el, currentElement) {
    $(currentElement).each(function () {
        $(this).find("select").each(function () {
            var parent = $(this).parent();
            var selectText = $(this).find("option").first().text();
            parent.addClass('dropdown-select');
            parent.append('<div class="dropdown-header">' + selectText + '<i class="fa fa-angle-down fa-3x pull-right" aria-hidden="true"></i></div>');
            parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
            $(this).find('option').each(function () {
                if ($(this).val() != "") {
                    parent.find("ul.dropdown-list").append('<li value="' + $(this).val() + '">' + $(this).val() + '</li>')
                }
            })
        })
    });
    $(currentElement).find('.dropdown-select.input .dropdown-header').click(function (event) {
        $(this).toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).children('.arrow-white').toggle();
    });
    $(currentElement).find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        var parent = $(this).parent();
        parent.siblings('.dropdown-header').html(choose + ' <i class="fa fa-angle-down fa-3x pull-right" aria-hidden="true"></i>');
        parent.siblings('select').find('option').removeAttr('selected');
        parent.siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        parent.find('li').removeClass('selected');
        $(this).addClass('selected');
        parent.siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $(currentElement).find('.dropdown-select .input').click(function (event) {
        event.stopPropagation();
    });

}