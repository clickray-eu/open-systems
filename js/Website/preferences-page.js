function sortEelements() {

    $(".email-prefs .item .fakelabel span:contains('English')").each(function() {
        $(this).parents().eq(3).addClass('english');
    })

    $(".email-prefs .item .fakelabel span:contains('German')").each(function() {
        $(this).parents().eq(3).addClass('german');
    })




    $(".email-prefs .item").wrapAll("<div class='flex-container'><div class='left'></div><div class='right'></div></div>");




    $(".email-prefs .item").each(function() {

        if ($(this).hasClass("german")) {
            $(this).appendTo(".left");
        } else {
            $(this).prependTo(".right");
        }

    })
}

$( document ).ready(function() {
   sortEelements();
});
