

$(".gallery-switch li a").on("click", function(event) {
	
    event.preventDefault();
    
    var header = $(".gallery-header h2");
    var headerText = $(this).attr("data-header");
	var currentTab = $(this).attr("href").toString();
    var lastChar = currentTab.length - 1;
    var tabNumber = currentTab[lastChar] - 1;
    
    header.text(headerText);
    
    $(".tab").not($(currentTab)).css("display","none");
    $(currentTab).fadeIn();

    if ($(this).parent().parent().hasClass("switch-bottom")) {
        var offset = $(".gallery-header").offset().top - 100;
        $('html,body').animate({
            scrollTop: offset
        }, 500);
    }

 })