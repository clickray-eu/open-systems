$(document).ready(function () {

    $('.slide-content-wrapper > span').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        responsive: [{
            breakpoint: 768,
            settings: {
                dots: false

            }
        }]
    });
    $('.magazines-slider__section').slick({
        autoplay: true,
        dots: true,
        arrows: false,
        infinite: true,
        autoplaySpeed: 7000,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [{
            breakpoint: 550,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });

});