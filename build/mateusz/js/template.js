"use strict";

// file: cookie-disclaimer.js
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

if (getCookie("cookieInfo") == "yes") {
    $("#cookie-info").css("display", "none");
}

$("#cookie-info a").click(function (e) {
    e.preventDefault();
    setCookie("cookieInfo", "yes", 360);
    $("#cookie-info").css("display", "none");
});
// end file: cookie-disclaimer.js

// file: event.js
function toTheTop() {
    var el = $('#back-to-top-arrow');
    var offset = $(window).scrollTop();
    var windowHeight = $(window).height();
    var windowWidth = $(window).innerWidth();
    var documentHeight = $(document).height();
    var sum = offset + windowHeight + 150;
    if (offset >= windowHeight + 100 && $(window).innerWidth() > 768 && sum <= documentHeight) {
        el.fadeIn();
    } else {
        el.fadeOut();
    }
}

function withdrawTweet(twitterIframe) {
    var tweetIndex = 0,
        tweetWrap = $(twitterIframe),
        tweetIframe = tweetWrap.find('iframe'),
        tweetWrapper = tweetIframe.contents().find('.timeline-TweetList-tweet'),
        tweetCustom = tweetWrap.find('.tweets-wrapper .tweet-content');

    tweetWrapper.each(function () {
        var tweetText = $(this).contents().find('.timeline-Tweet-text').html(),
            tweetDate = $(this).contents().find('.timeline-Tweet-metadata time').attr('datetime').split(/T|\+|\-|\:/),
            tweetAvatarSrc = $(this).contents().find('.Avatar.Avatar--edge').attr('src'),
            tweetName = $(this).contents().find('.TweetAuthor-name').html(),
            tweetNameShort = $(this).contents().find('.TweetAuthor-screenName').html(),
            tweetDateString = tweetDate[1] + '/' + tweetDate[2] + '/' + tweetDate[0][2] + tweetDate[0][3] + ', ' + tweetDate[3] + ':' + tweetDate[4];
        tweetCustom.eq(tweetIndex).find('.tweet-name').html(tweetName);
        tweetCustom.eq(tweetIndex).find('.tweet-name-short').html(tweetNameShort);
        tweetCustom.eq(tweetIndex).find('.tweet-post p').html(tweetText);
        tweetCustom.eq(tweetIndex).find('.tweet-date p').html(tweetDateString);
        tweetCustom.eq(tweetIndex).find('.tweet-avatar img').attr('src', tweetAvatarSrc);

        tweetIndex++;
    });
    tweetWrap.find('.loader').fadeOut();
}

function getScrollBarWidth() {
    var $outer = $('<div>').css({ visibility: 'hidden', width: 100, overflow: 'scroll' }).appendTo('body'),
        widthWithScroll = $('<div>').css({ width: '100%' }).appendTo($outer).outerWidth();
    $outer.remove();
    return 100 - widthWithScroll;
};

function matchHeight(elDesiredHeight, elToChange) {
    if ($(window).innerWidth() > 767) {
        var wantedHeight = $(elDesiredHeight).height();
        $(elToChange).height(wantedHeight);
    } else {
        $(elToChange).height('auto');
    }
}
$(window).load(function () {
    var interval = setInterval(function () {
        if ($('.twitter-box iframe').contents().find('.timeline-TweetList-tweet').length > 0) {
            clearInterval(interval);
            withdrawTweet('.twitter-box');
        }
    }, 50);
});

$(document).ready(function () {
    "use strict";

    toTheTop();

    if ($('body').hasClass('stories')) {
        $('.os-menu ul li a').filter(function () {
            return $(this).text() === 'Stories';
        }).parent().addClass('active-branch');
    }
    if ($('body').hasClass('about')) {
        $('.os-menu ul li a').filter(function () {
            return $(this).text() === 'About';
        }).parent().addClass('active-branch');
    }
    if ($('.blog-stories-single:not(.about-single)')) {
        setTimeout(function () {
            $(window).on('resize', function () {
                matchHeight('.blog-content', '.blog-sidebar-left');
            }).resize();
        }, 50);
    }

    if ($('.fancybox')) {
        $('.fancybox').fancybox({
            padding: 0,
            onStart: function onStart() {
                if ($(document).width() > $(window).width()) {
                    $('body').addClass('fancybox-lock');
                    $('body').addClass('fancybox-margin');
                } else {
                    $('body').addClass('fancybox-lock');
                }
            },
            onClosed: function onClosed() {
                $('body').removeClass('fancybox-lock fancybox-margin');
            }
        });
    }

    $(window).scroll(function () {
        toTheTop();
    });

    $('#back-to-top, #back-to-top-arrow').on('click', function () {
        $('body, html').animate({
            scrollTop: 0
        }, 500);
    });
    if ($('.sidebar .subnav')) {
        $('.sidebar .subnav ul li a').each(function () {
            var txt = $(this).text();
            $(this).html('<span>' + txt + '</span>');
        });
    }
    // ------ scroll do # w linkach ------


    $('a[href*="#"]:not([href="#"]):not(.topic-link):not(.switch)').on('click', function (e) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 500);
                return false;
            }
        }
    });
    // ------ scroll do # w linkach end ------
});

// end file: event.js

// file: form.js
// Functions for dropdown
function select(el, currentElement) {
    $(currentElement).each(function () {
        $(this).find("select").each(function () {
            var parent = $(this).parent();
            var selectText = $(this).find("option").first().text();
            parent.addClass('dropdown-select');
            parent.append('<div class="dropdown-header">' + selectText + '<i class="fa fa-angle-down fa-3x pull-right" aria-hidden="true"></i></div>');
            parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
            $(this).find('option').each(function () {
                if ($(this).val() != "") {
                    parent.find("ul.dropdown-list").append('<li value="' + $(this).val() + '">' + $(this).val() + '</li>');
                }
            });
        });
    });
    $(currentElement).find('.dropdown-select.input .dropdown-header').click(function (event) {
        $(this).toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).children('.arrow-white').toggle();
    });
    $(currentElement).find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        var parent = $(this).parent();
        parent.siblings('.dropdown-header').html(choose + ' <i class="fa fa-angle-down fa-3x pull-right" aria-hidden="true"></i>');
        parent.siblings('select').find('option').removeAttr('selected');
        parent.siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        parent.find('li').removeClass('selected');
        $(this).addClass('selected');
        parent.siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $(currentElement).find('.dropdown-select .input').click(function (event) {
        event.stopPropagation();
    });
}
// end file: form.js

// file: global.js
function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}

function hideEmptyLabel() {
    $('form label').each(function (i, e) {
        if ($(this).text() == "*") {
            $(this).css('display', 'none');
        }
    });
}

waitForLoad(".form, .widget-type-form, .widget-type-blog_content, .hs_cos_wrapper_type_form, .widget-type-blog_subscribe, .widget-type-blog_comments", "form", hideEmptyLabel);
waitForLoad(".form, .widget-type-form, .widget-type-blog_content, .subscribe-module", "form", select);

$(window).load(function () {
    formPopup('.btn-subscribe', '.subscribe-popup');
    formPopup('.btn-contact-sales', '.contact-sales-popup');
    formPopup('.btn-contact-footer', '.contact-footer-popup');
    formPopup('.btn-contact', '.contact-footer-popup');
});

// end file: global.js

// file: header.js
$(document).ready(function () {

    slickInit();
    stickyNav();
});

function slickInit() {

    var combinedMenu = $('.menu-left .hs-menu-wrapper > ul').clone();
    var secondMenu = $('.menu-right .hs-menu-wrapper > ul').clone();

    secondMenu.children('li').appendTo(combinedMenu);

    combinedMenu.slicknav({
        duplicate: false,
        label: '',
        prependTo: '.os-menu-wrapper .container',
        openedSymbol: '<i class="fa fa-angle-down" aria-hidden="true"></i>',
        closedSymbol: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        removeClasses: true,
        brand: 'Open Systems',
        allowParentLinks: true
    });
}

function stickyNav() {
    var menuOffsetTop = $('.os-menu-wrapper').offset().top;
    var menuHeight = $('.os-menu-wrapper').outerHeight();
    $('.os-menu-wrapper').parent().css('height', menuHeight);

    $(window).resize();
    if (menuOffsetTop > 0 && $(window).width() > 991) {
        var menuItem = $('.os-menu-wrapper .hs-menu-depth-2.hs-item-has-children > ul');
        var menuItemParentWidth = menuItem.parent().width();

        $(document).scroll(function () {
            var y = $(this).scrollTop();

            if (y > menuOffsetTop) {
                $('.os-menu-wrapper').addClass('sticky');
            } else {
                $('.os-menu-wrapper').removeClass('sticky');
            }
        }).scroll();
    } else {
        console.log();
        $('.os-menu-wrapper').addClass('sticky');
        $('.slide-content-wrapper').css('margin-top', menuHeight);
        // $('.os-menu-wrapper').parent().css('max-height', 0);

    }
}
// end file: header.js

// file: Blog/blog.js
$(".blog .form-wrapper .exit-btn").click(function () {
    $(this).parent().parent().hide(500);
});
// end file: Blog/blog.js

// file: Modules/form-popup.js
function formPopup(button, modal) {
    var button = $(button),
        modal = $(modal),
        form = $('.subscribe-module .form-wrapper'),
        exit = $('.exit-btn'),
        body = $('body'),
        afterLoad = $('a[name="' + location.hash.replace('#', '') + '"]');

    $(button).on('click', function (e) {
        e.preventDefault;
        modal.fadeIn();
        body.css({
            'overflow': 'hidden'
        });
    });

    $(exit).on('click', function () {
        modal.fadeOut();
        body.css({
            'overflow': 'scroll'
        });
    });

    form.on('click', function (e) {
        e.stopPropagation();
    });

    if ($('a[name="' + location.hash.replace('#', '') + '"]')) {
        afterLoad.closest(modal).fadeIn(700);
    }
}

// end file: Modules/form-popup.js

// file: Website/about-events.js
function checkEventDate() {

    var today = new Date();

    $(".simple-single-event").each(function () {

        var endEventDate = $(this).attr("data-date-end");
        var endDate = new Date(endEventDate);

        console.log(endDate);
        console.log(today);
        console.log('-------------------------------');

        if (endDate < today) {
            $(this).addClass('event-end');
        }
    });
}

checkEventDate();
// end file: Website/about-events.js

// file: Website/about-main.js


$(".gallery-switch li a").on("click", function (event) {

    event.preventDefault();

    var header = $(".gallery-header h2");
    var headerText = $(this).attr("data-header");
    var currentTab = $(this).attr("href").toString();
    var lastChar = currentTab.length - 1;
    var tabNumber = currentTab[lastChar] - 1;

    header.text(headerText);

    $(".tab").not($(currentTab)).css("display", "none");
    $(currentTab).fadeIn();

    if ($(this).parent().parent().hasClass("switch-bottom")) {
        var offset = $(".gallery-header").offset().top - 100;
        $('html,body').animate({
            scrollTop: offset
        }, 500);
    }
});
// end file: Website/about-main.js

// file: Website/home.js
$(document).ready(function () {

    $('.slide-content-wrapper > span').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        responsive: [{
            breakpoint: 768,
            settings: {
                dots: false

            }
        }]
    });
    $('.magazines-slider__section').slick({
        autoplay: true,
        dots: true,
        arrows: false,
        infinite: true,
        autoplaySpeed: 7000,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [{
            breakpoint: 550,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
});
// end file: Website/home.js

// file: Website/preferences-page.js
function sortEelements() {

    $(".email-prefs .item .fakelabel span:contains('English')").each(function () {
        $(this).parents().eq(3).addClass('english');
    });

    $(".email-prefs .item .fakelabel span:contains('German')").each(function () {
        $(this).parents().eq(3).addClass('german');
    });

    $(".email-prefs .item").wrapAll("<div class='flex-container'><div class='left'></div><div class='right'></div></div>");

    $(".email-prefs .item").each(function () {

        if ($(this).hasClass("german")) {
            $(this).appendTo(".left");
        } else {
            $(this).prependTo(".right");
        }
    });
}

$(document).ready(function () {
    sortEelements();
});

// end file: Website/preferences-page.js

// file: Website/sitemap.js
$(document).ready(function () {
    $(".sitemap-menu").find('br').replaceWith(' ');
    $(".sitemap-menu").find('i').remove();
});

// end file: Website/sitemap.js
//# sourceMappingURL=template.js.map
